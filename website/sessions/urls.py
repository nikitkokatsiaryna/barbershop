from django.urls import path
from .views import SessionsView, SessionDeleteView

urlpatterns = [
    path('', SessionsView.as_view(), name='index'),
    path('delete/', SessionDeleteView.as_view(), name='delete')
]
