from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from website.models import Staff, Service, Entry
from django.forms.forms import Form


class SessionsView(TemplateView):
    template_name = "sessions/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entries'] = Entry.objects.filter(user=self.request.user)
        return context


class SessionDeleteView(FormView):
    form_class = Form
    success_url = "/sessions/"

    def form_valid(self, form):
        Entry.objects.filter(
            user=self.request.user,
            staff=Staff.objects.get(id=self.request.POST.get('staff_id')),
            service=Service.objects.get(id=self.request.POST.get('service_id'))
        ).delete()

        return super().form_valid(form)
