from django.db import models
from django.contrib.auth.models import User


class Service(models.Model):
    name = models.CharField(max_length=255)
    cost = models.DecimalField(max_digits=7, decimal_places=2)


class Staff(models.Model):
    name = models.CharField(max_length=100)


class Entry(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    service = models.ForeignKey(to=Service, on_delete=models.SET_NULL, null=True)
    staff = models.ForeignKey(to=Staff, on_delete=models.SET_NULL, null=True)
