from django.views.generic import TemplateView


class ProfileDetailView(TemplateView):
    template_name = 'profile/profile_detail.html'
