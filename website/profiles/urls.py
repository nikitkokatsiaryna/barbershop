from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import ProfileDetailView

urlpatterns = [
    path('profile/details/', login_required(ProfileDetailView.as_view()), name='profile_details')
]
