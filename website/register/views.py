from django.views.generic.edit import FormView
from .forms import RegisterForm


class RegisterView(FormView):
    form_class = RegisterForm
    success_url = "/login/"
    template_name = "register/index.html"

    def form_valid(self, form):
        form.save()

        return super(RegisterView, self).form_valid(form)
