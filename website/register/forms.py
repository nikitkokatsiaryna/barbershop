from django import forms
from django.forms import TextInput, EmailInput, PasswordInput
from django.contrib.auth.models import User as AuthUser
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import password_validation


class RegisterForm(UserCreationForm):
    password1 = forms.CharField(
        widget=PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Пароль'}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        widget=PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Повторите пароль'}),
        help_text=_("Enter the same password as before, for verification."),
    )

    class Meta:
        model = AuthUser
        fields = ('username', 'email', 'first_name', 'password1', 'password2')
        widgets = {
            'username': TextInput(attrs={'class': 'form-control', 'placeholder': 'Логин'}),
            'email': EmailInput(attrs={'class': 'form-control', 'placeholder': 'Эл.почта'}),
            'first_name': TextInput(attrs={'class': 'form-control', 'placeholder': 'Имя'})
        }
