from django.test import TestCase, Client
from . import defines


class UrlsTest(TestCase):
    def setUP():
        self.client = Client()

    def test_home_page_status(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, defines.HTTP_200_OK)

    def test_online_entry_services_status(self):
        response = self.client.get('/online_entry/choose_service/')
        self.assertEqual(response.status_code, defines.HTTP_200_OK)

    def test_online_entry_staff_status(self):
        response = self.client.get('/online_entry/choose_staff/')
        self.assertEqual(response.status_code, defines.HTTP_200_OK)


class RedirectTest(TestCase):
    def setUp():
        self.client = Client

    def test_online_staff_status(self):
        response = self.client.get('/online_entry/choose_service/')
        self.assertRedirects(response, '/online_entry/choose_service/', status_code=302, target_status_code=200)

    def test_online_staff_status(self):
        response = self.client.get('/online_entry/choose_service/')
        self.assertRedirects(response, '/online_entry/choose_staff/', status_code=302, target_status_code=200)
