from django.views.generic.edit import FormView
from .forms import LoginForm
from django.contrib.auth import login


class LoginView(FormView):
    form_class = LoginForm
    success_url = '/'
    template_name = "login/index.html"

    def form_valid(self, form):
        self.user = form.get_user()

        login(self.request, self.user)
        return super().form_valid(form)
