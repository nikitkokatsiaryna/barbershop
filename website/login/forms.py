from django import forms
from django.forms import PasswordInput, CharField
from django.contrib.auth.models import User as AuthUser
from django.contrib.auth.forms import AuthenticationForm


class LoginForm(AuthenticationForm):
    username = CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Логин', 'autofocus': True}))
    password = forms.CharField(
        strip=False,
        widget=PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Пароль'})
    )

    class Meta:
        model = AuthUser
        fields = ('username', 'password')

