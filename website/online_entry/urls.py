from django.urls import path
from .views import ServicesFormView, StaffFormView
from django.views.generic import TemplateView

urlpatterns = [
    path('choose_service/', ServicesFormView.as_view(), name='choose_service'),
    path('choose_staff/', StaffFormView.as_view(template_name='online_entry/choose_staff.html'), name='choose_staff'),
    # path('choose_date/', TemplateView.as_view(template_name='online_entry/choose_date.html'), name='choose_date')
]
