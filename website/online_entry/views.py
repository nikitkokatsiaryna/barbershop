from django.views.generic.edit import FormView
from .forms import ServicesForm, StaffForm
from website.models import Service, Staff, Entry
from django.contrib.auth.models import User


class ServicesFormView(FormView):
    form_class = ServicesForm
    success_url = '/online_entry/choose_staff/'
    template_name = "online_entry/choose_service.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['services'] = Service.objects.all()
        return context

    def form_valid(self, form):
        self.request.session['service_ids'] = self.request.POST.getlist('services[]')

        return super().form_valid(form)


class StaffFormView(FormView):
    form_class = StaffForm
    success_url = '/sessions/'
    template_name = 'online_entry/choose_staff.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['staff'] = Staff.objects.all()
        return context

    def form_valid(self, form):
        user_id = self.request.user.id
        staff_id = self.request.POST['staff']
        service_ids = self.request.session['service_ids']

        for service_id in service_ids:
            Entry.objects.create(
                user=User.objects.get(id=user_id),
                service=Service.objects.get(id=service_id),
                staff=Staff.objects.get(id=staff_id))

        return super().form_valid(form)
