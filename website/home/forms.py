from django import forms
from website import models


class ServicesForm(forms.ModelForm):
    model = models.Service
    fields = '__all__'


class TeamForm(forms.ModelForm):
    model = models.Staff
    fields = '__all__'
