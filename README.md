# Barbershop

Barbershop for true men. Inspired by British history and traditions, we have created a place where they honor the classic
old school of male haircuts and marriages. A place where anyone, from a child to a respectable man, will find what
looking for.

### Install postgres
First need to install DB PostgreSQL:

```
brew install postgres
initdb /usr/local/var/barbershop
createdb barbershop
psql -d barbershop -c "CREATE ROLE barbershop WITH LOGIN password 'barbershop'"
```

### Install project

```
git clone git@gitlab.com:nikitkokatsiaryna/barbershop.git
cd barbershop
python -m venv env
source env/bin/activate
pip install -r requirements.txt
./manage.py migrate;  ./manage.py loaddata website/fixture_data.json


```
### Running

```
./manage.py runserver

```
