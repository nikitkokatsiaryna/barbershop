from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LogoutView

import website.home.urls as home_urls
import website.about.urls as about_urls
import website.online_entry.urls as online_entry_urls
import website.profiles.urls as profiles_urls
import website.services.urls as services_urls
import website.staff.urls as staff_urls
import website.contacts.urls as contacts_urls
import website.sessions.urls as sessions_urls
import website.register.urls as register_urls
import website.login.urls as login_urls


urlpatterns = [
    path('', include((home_urls, 'website.home'), namespace='home')),
    path('online_entry/', include((online_entry_urls, 'website.online_entry'), namespace='online_entry')),
    path('profiles/', include((profiles_urls, 'website.profiles'), namespace='profiles')),
    path('about/', include((about_urls, 'website.about'), namespace='about')),
    path('services/', include((services_urls, 'website.services'), namespace='services')),
    path('staff/', include((staff_urls, 'website.staff'), namespace='staff')),
    path('contacts/', include((contacts_urls, 'website.contacts'), namespace='contacts')),
    path('sessions/', include((sessions_urls, 'website.sessions'), namespace='sessions')),
    path('register/', include((register_urls, 'website.register'), namespace='register')),
    path('login/', include((login_urls, 'website.login'), namespace='login')),
    path('logout/', LogoutView.as_view(template_name='home/index.html'), name='logout'),
    path('admin/', admin.site.urls)
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
